package ar.edu.unq.uis.rankit

import org.junit.Test
import org.junit.Assert
import ar.edu.unq.uis.rankit.evaluables.Evaluable
import ar.edu.unq.uis.rankit.evaluables.BuscarPorNombre
import java.time.LocalDate

/**
 * Created by Sarappa Carla on 21/09/16 10:02.
 */
class BuscadorTest {

        @Test
        def void cuandoSeBuscaUnaPalabra_SeEncuentranResultados(){
            val evaluable = new Evaluable("carla", LocalDate.now)
            val buscador = new BuscarPorNombre
            buscador.nombre = "carla"

            Assert.assertTrue(buscador.cumpleCondicionDeBusqueda(evaluable))
        }

        @Test
        def void cuandoSeBuscaPalabraVacia_SeEncuentranResultados(){
            val evaluable = new Evaluable("", LocalDate.now)
            val buscador = new BuscarPorNombre
            buscador.nombre = ""
            Assert.assertTrue(buscador.cumpleCondicionDeBusqueda(evaluable))

        }

        @Test
        def void cuandoSeBuscaUnaPalabraQueNoExiste_NoSeEncuentranResultados(){
            val evaluable = new Evaluable("Carla", LocalDate.now)
            val buscador = new BuscarPorNombre
            buscador.nombre = "x"
            Assert.assertFalse(buscador.cumpleCondicionDeBusqueda(evaluable))
        }

        @Test
        def void cuandoSeBuscaPorElComienzoDeUnaPalabra_SeMuestranResultados() {
            val evaluable = new Evaluable("Carla", LocalDate.now)
            val buscador = new BuscarPorNombre
            buscador.nombre = "c"
            Assert.assertTrue(buscador.cumpleCondicionDeBusqueda(evaluable))
        }


        @Test
        def void cuandoSeBuscaPorElMedioDeUnaPalabra_SeMuestranResultados() {
            val evaluable = new Evaluable("Carla", LocalDate.now)
            val buscador = new BuscarPorNombre
            buscador.nombre = "ar"
            Assert.assertTrue(buscador.cumpleCondicionDeBusqueda(evaluable))
        }


        @Test
        def void cuandoSeBuscaPorElFinalDeUnaPalabra_SeMuestranResultados() {
            val evaluable = new Evaluable("Carla", LocalDate.now)
            val buscador = new BuscarPorNombre
            buscador.nombre = "la"
            Assert.assertTrue(buscador.cumpleCondicionDeBusqueda(evaluable))
        }

    }