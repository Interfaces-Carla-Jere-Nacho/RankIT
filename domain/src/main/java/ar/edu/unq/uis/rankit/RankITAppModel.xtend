package ar.edu.unq.uis.rankit

import ar.edu.unq.uis.rankit.calificaciones.AdmCalificacionAppModel
import ar.edu.unq.uis.rankit.usuarios.AdmUsuarioAppModel
import org.uqbar.commons.utils.Observable
import org.eclipse.xtend.lib.annotations.Accessors
import ar.edu.unq.uis.rankit.usuarios.RepoUsuarios
import ar.edu.unq.uis.rankit.calificaciones.RepoCalificaciones
import ar.edu.unq.uis.rankit.evaluables.AdmEvaluableAppModel
import ar.edu.unq.uis.rankit.evaluables.RepoEvaluable

@Observable
@Accessors
class RankItAppModel {
    AdmUsuarioAppModel usuarios
    AdmCalificacionAppModel calificaciones
    AdmEvaluableAppModel lugares
    AdmEvaluableAppModel servicios

    new(RepoUsuarios repoUsuarios, RepoCalificaciones repoCalificaciones,
    RepoEvaluable repoLugares, RepoEvaluable repoServicios)
    {
        calificaciones = new AdmCalificacionAppModel(repoCalificaciones);
        usuarios = new AdmUsuarioAppModel(repoUsuarios, calificaciones);
        lugares = new AdmEvaluableAppModel(repoLugares, calificaciones)
        servicios = new AdmEvaluableAppModel(repoServicios, calificaciones)

    }
}
