package ar.edu.unq.uis.rankit

import ar.edu.unq.uis.rankit.evaluables.*
import java.util.List
import java.util.Set

/**
 * Created by Sarappa Carla on 21/09/16 16:48.
 */
abstract class BaseRepository<T> {
    protected Set<T> elementos

    new(List<T> elementos) {
        this.elementos = elementos.toSet
    }


    def List<T> find((T) => Boolean condicion) {

       elementos.filter [ condicion.apply(it) ].toList
    }

    def eliminar(T elemento) {
        elementos.remove(elemento)
    }

    def T agregarPorDefault()


    def guardar(T elemento){
        elementos.add(elemento)
    }
}