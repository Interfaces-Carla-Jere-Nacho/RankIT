package ar.edu.unq.uis.rankit.evaluables

import java.util.List
import java.time.LocalDate
import ar.edu.unq.uis.rankit.BaseRepository

class RepoEvaluable extends BaseRepository<Evaluable> {


    new(List<Evaluable> evaluables) {
        super(evaluables)
    }


    override def agregarPorDefault() {
        val nuevoElemento = new Evaluable("X", LocalDate.now)
        elementos.add(nuevoElemento)
        nuevoElemento
    }



}