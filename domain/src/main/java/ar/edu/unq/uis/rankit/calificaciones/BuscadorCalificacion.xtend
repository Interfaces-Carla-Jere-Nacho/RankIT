package ar.edu.unq.uis.rankit.calificaciones

/**
 * Created by Sarappa Carla on 21/09/16.
 */

import org.uqbar.commons.utils.Observable
import org.eclipse.xtend.lib.annotations.Accessors
import ar.edu.unq.uis.rankit.evaluables.BuscarPorNombre
import ar.edu.unq.uis.rankit.evaluables.Evaluable

@Observable
@Accessors
class BuscadorCalificacion {

    String user = ""
    String evaluado = ""

    def correspondeCalificacion (Calificacion calificacion) {
        (!ingresoUsuario && !ingresoEvaluado)
                ||
                (!ingresoEvaluado && ingresoUsuario &&
                        calificacion.user.nombre.toLowerCase().equals(this.user.toLowerCase()))
                ||
                (!ingresoUsuario && ingresoEvaluado &&
                        calificacion.evaluado.nombre.toLowerCase().equals(this.evaluado.toLowerCase()))
                ||
                (calificacion.user.nombre.toLowerCase().equals(this.user.toLowerCase()) &&
                        calificacion.evaluado.nombre.toLowerCase().equals(this.evaluado.toLowerCase())
                )
    }


    def ingresoUsuario() {
        user != null && !user.equals("")
    }

    def ingresoEvaluado(){
        evaluado != null && !evaluado.equals("")
    }
}