package ar.edu.unq.uis.rankit.calificaciones

import java.util.ArrayList
import java.util.List
import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.commons.utils.Observable
import ar.edu.unq.uis.rankit.usuarios.Usuario
import java.time.LocalDate
import ar.edu.unq.uis.rankit.evaluables.Evaluable
import ar.edu.unq.uis.rankit.evaluables.BuscarPorNombre

@Observable
@Accessors
class AdmCalificacionAppModel {
    List<Calificacion> calificaciones
    RepoCalificaciones repoCalificaciones
    BuscadorCalificacion buscadorCalificacion
    Calificacion calificacionSeleccionada
    int registradas

    new(RepoCalificaciones repoCalificaciones) {
        buscadorCalificacion = new BuscadorCalificacion
        calificaciones = new ArrayList<Calificacion>
        this.repoCalificaciones = repoCalificaciones
    }

    def void buscar() {
        val resultado = buscarPorUsuario()
        resultado.retainAll(buscarPorEvaluable())
        calificaciones = resultado
    }

    def buscarPorUsuario(){
        repoCalificaciones.find([ c |
            val buscador = new BuscarPorNombre
            buscador.nombre = buscadorCalificacion.user
            buscador.apply(c.user)
        ])
    }

    def buscarPorEvaluable(){
        repoCalificaciones.find([ c |
            val buscador = new BuscarPorNombre
            buscador.nombre = buscadorCalificacion.evaluado
            buscador.apply(c.evaluado)
        ])
    }


    def eliminarCalificacion() {
        repoCalificaciones.eliminar(calificacionSeleccionada)
        this.buscar
        registradas = getRegistradas
    }


    def nuevaCalificacion(){
        repoCalificaciones.agregarPorDefault()
        this.buscar
        registradas = getRegistradas
    }

    def getRegistradas(){
        calificaciones.length
    }

    def getOfensivas(){
        var List<Calificacion> registradasAux = calificaciones.filter [it.esOfensiva].toList
        registradasAux.size
    }

    def getInofensivas(){
        getRegistradas - getOfensivas
    }

    def filtrarPorUsuario(Usuario usuario){
        buscadorCalificacion.evaluado = ""
        buscadorCalificacion.user = usuario.nombre
        this.buscar
    }

    def filtrarPorEvaluado(Evaluable evaluado){
        buscadorCalificacion.user = ""
        buscadorCalificacion.evaluado = evaluado.nombre
        this.buscar
    }

}