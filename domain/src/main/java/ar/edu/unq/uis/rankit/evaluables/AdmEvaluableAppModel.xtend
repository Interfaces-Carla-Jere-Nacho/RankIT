package ar.edu.unq.uis.rankit.evaluables


import java.util.List
import java.util.ArrayList
import org.uqbar.commons.utils.Observable
import org.eclipse.xtend.lib.annotations.Accessors
import ar.edu.unq.uis.rankit.calificaciones.AdmCalificacionAppModel
import ar.edu.unq.uis.rankit.appModels.BaseAppModel
import org.uqbar.commons.model.ObservableUtils

@Observable
@Accessors
class AdmEvaluableAppModel extends BaseAppModel<Evaluable>{

    AdmCalificacionAppModel calificaciones
    List<Evaluable> evaluables
    RepoEvaluable repo
    BuscarPorNombre buscadorEvaluable
    Evaluable evaluableSeleccionado
    int inscriptos


    def void setEvaluableSeleccionado(Evaluable e){
        evaluableSeleccionado = e
        ObservableUtils.firePropertyChanged(this, "evaluableSeleccionadoHabilitado")
    }
    new(RepoEvaluable repo, AdmCalificacionAppModel calificaciones) {
        buscadorEvaluable = new BuscarPorNombre
        evaluables = new ArrayList<Evaluable>
        this.repo = repo
        this.calificaciones = calificaciones
    }

    def void buscar() {
        evaluables = repo.find(buscadorEvaluable)
    }

    def eliminarEvaluable() {
        repo.eliminar(evaluableSeleccionado)
        ObservableUtils.firePropertyChanged(this, "inscriptos")
    }

    override def nuevaEntidad(){
        repo.agregarPorDefault()
        this.buscar
        inscriptos = getInscriptos
    }

    def getInscriptos(){
        evaluables.length
    }

    def getHabilitados(){
        var List<Evaluable> habilitadosAux = evaluables.filter [it.habilitado].toList
        habilitadosAux.size
    }

    def getDeshabilitados(){
        getInscriptos - getHabilitados
    }

    def filtrarCalificaciones(){
        calificaciones.filtrarPorEvaluado(evaluableSeleccionado)

    }

    def Boolean getEvaluableSeleccionadoHabilitado(){
        this.evaluableSeleccionado.habilitado
    }

    def void setEvaluableSeleccionadoHabilitado(Boolean seleccionado){
        this.evaluableSeleccionado.habilitado = seleccionado
        ObservableUtils.firePropertyChanged(this, "habilitados")
        ObservableUtils.firePropertyChanged(this, "deshabilitados")
    }

}