package ar.edu.unq.uis.rankit.usuarios

import java.util.List
import java.time.LocalDate
import ar.edu.unq.uis.rankit.BaseRepository

class RepoUsuarios extends BaseRepository<Usuario>{

    new(List<Usuario> usuarios) {
        super(usuarios)
    }


    def getUsuario(Usuario usuario) {
        elementos.findFirst [ it.nombre.equals(usuario.nombre)]
    }



    override def agregarPorDefault() {
        val nuevoUsuarioPorDefault = new Usuario("NN", LocalDate.now)
        elementos.add(nuevoUsuarioPorDefault)
        nuevoUsuarioPorDefault
    }

}