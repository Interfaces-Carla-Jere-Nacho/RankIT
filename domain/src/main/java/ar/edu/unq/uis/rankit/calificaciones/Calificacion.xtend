package ar.edu.unq.uis.rankit.calificaciones

import java.time.LocalDate
import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.commons.utils.Observable
import ar.edu.unq.uis.rankit.usuarios.Usuario
import ar.edu.unq.uis.rankit.evaluables.Evaluable

@Observable
@Accessors
class Calificacion{
    Usuario user
    Evaluable evaluado
    int puntos
    String fechaRegistro
    String detalle
    boolean esOfensiva

    new(Usuario user, Evaluable evaluado, int puntos, LocalDate fecha, String detalle){
        this.user = user
        this.evaluado = evaluado
        this.puntos = puntos
        this.detalle = detalle
        this.fechaRegistro = fecha.toString
        this.esOfensiva = false
    }

    def void setEsOfensiva(boolean bool){
        esOfensiva = bool
    }
}