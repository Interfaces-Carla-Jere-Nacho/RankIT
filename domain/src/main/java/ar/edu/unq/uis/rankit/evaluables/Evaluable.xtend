package ar.edu.unq.uis.rankit.evaluables

import java.time.LocalDate
import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.commons.utils.Observable

@Observable
@Accessors
class Evaluable implements Cloneable {
    String fechaRegistro
    String nombre
    Boolean habilitado

    new(String nombre, LocalDate fecha){
        this.nombre = nombre
        this.fechaRegistro = fecha.toString
        this.habilitado = false
    }

    def copy() {
        super.clone as Evaluable
    }
}