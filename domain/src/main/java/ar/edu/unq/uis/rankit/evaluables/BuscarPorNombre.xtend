package ar.edu.unq.uis.rankit.evaluables

import org.uqbar.commons.utils.Observable
import org.eclipse.xtend.lib.annotations.Accessors

@Observable
@Accessors
class BuscarPorNombre implements (Evaluable)=>Boolean {

    String nombre = ""

    def cumpleCondicionDeBusqueda (Evaluable evaluable) {
        (nombre.isEmpty || evaluable.nombre.toLowerCase().contains(this.nombre.toLowerCase()))
    }

    override def apply(Evaluable evaluable){
        cumpleCondicionDeBusqueda(evaluable)
    }

}