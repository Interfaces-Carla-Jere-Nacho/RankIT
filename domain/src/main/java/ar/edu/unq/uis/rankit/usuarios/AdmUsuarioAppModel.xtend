package ar.edu.unq.uis.rankit.usuarios

import java.util.ArrayList
import java.util.List
import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.commons.utils.Observable
import ar.edu.unq.uis.rankit.calificaciones.AdmCalificacionAppModel
import ar.edu.unq.uis.rankit.evaluables.BuscarPorNombre
import org.uqbar.commons.utils.Dependencies
import org.uqbar.commons.model.ObservableUtils
import ar.edu.unq.uis.rankit.appModels.BaseAppModel

@Observable
@Accessors
class AdmUsuarioAppModel extends BaseAppModel<Usuario>{

    AdmCalificacionAppModel calificaciones
    List<Usuario> usuarios
    RepoUsuarios repoUsuarios
    BuscarPorNombre buscaUsuario
    Usuario usuarioSeleccionado
    int registrados

    new(RepoUsuarios repo, AdmCalificacionAppModel calificaciones) {
        buscaUsuario = new BuscarPorNombre
        usuarios = new ArrayList
        this.repoUsuarios = repo
        this.calificaciones = calificaciones
    }

    def void buscar() {
        usuarios = repoUsuarios.find(buscaUsuario)
    }

    def blanquearClave(){
        repoUsuarios.getUsuario(usuarioSeleccionado).setPassword("123")
    }

    def eliminar() {
        repoUsuarios.eliminar(usuarioSeleccionado)
        this.buscar
        registrados = getRegistrados
        this.usuarioSeleccionado = usuarios.get(0)

    }

    override def nuevaEntidad(){
        repoUsuarios.agregarPorDefault()
        this.buscar
        registrados = getRegistrados
    }

    def getRegistrados(){
        usuarios.length
    }

    def getActivos(){
        usuarios.filter [it.activo].toList.size
    }

    def getInactivos(){
        getRegistrados - getActivos
    }

    @Dependencies("usuarios")
    def getBaneados(){
        var List<Usuario> baneadosAux = usuarios.filter [it.baneado].toList
        baneadosAux.size
    }

    def filtrarCalificaciones(){
        calificaciones.filtrarPorUsuario(usuarioSeleccionado)
    }

    def void setBaneado(Boolean bool){
        usuarioSeleccionado.baneado = bool
        ObservableUtils.firePropertyChanged(this, "activos", getActivos())

    }
    def Boolean getBaneado(){
        usuarioSeleccionado.baneado
    }
}