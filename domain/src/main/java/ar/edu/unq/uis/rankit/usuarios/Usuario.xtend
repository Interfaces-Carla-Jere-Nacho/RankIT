package ar.edu.unq.uis.rankit.usuarios

import java.time.LocalDate
import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.commons.utils.Observable
import org.uqbar.commons.utils.Dependencies
import ar.edu.unq.uis.rankit.evaluables.Evaluable


@Observable
@Accessors
class Usuario extends Evaluable {
    String password
    Boolean baneado

    new(String nombre, LocalDate fecha){
        super(nombre, fecha)
        this.baneado = false
        this.password = "123"
    }

    /**
     * getActivo() es una propiedad calculada y depende de otras dos propiedades.
     *
     * Internamente lo que hace es que cuando cambia una propiedad, actualiza todas aquellas propiedades que
     * dependen de ella (el annotation Dependencies llama a firePropertyChanged de cada una de las dependencias)
     * Esto es correcto que vaya en el modelo porque el mismo no conoce (ni debe conocer) al panel de edición.
     *
     * Ver https://sites.google.com/site/programacionui/temario/02-disenio-UI/arena-binding-avanzado
     */
    @Dependencies("habilitado", "baneado")
    def Boolean getActivo(){
        this.habilitado && !baneado
    }

    def void setActivo(Boolean bool){
        this.habilitado = bool;
    }

    @Dependencies("baneado")
    def Boolean getNoBaneado(){
        !baneado
    }

    def ultimaFechaDePublicacion(){
        LocalDate.now
    }
}