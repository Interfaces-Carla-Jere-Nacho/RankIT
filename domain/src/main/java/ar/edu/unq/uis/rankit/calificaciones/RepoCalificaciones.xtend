package ar.edu.unq.uis.rankit.calificaciones


import java.time.LocalDate
import java.util.List
import ar.edu.unq.uis.rankit.usuarios.Usuario
import ar.edu.unq.uis.rankit.evaluables.Evaluable
import ar.edu.unq.uis.rankit.BaseRepository

class RepoCalificaciones extends BaseRepository<Calificacion>{

    new(List<Calificacion> calificaciones) {
        super(calificaciones)
    }

    def nuevaCalificacion(Usuario user, Evaluable evaluado, int puntos, LocalDate fecha, String detalle){
        elementos.add(new Calificacion(user, evaluado, puntos, fecha, detalle))
    }


    override def agregarPorDefault() {
        val usuario = new Usuario("ADMIN", LocalDate.now)
        val evaluado = new Evaluable("X", LocalDate.now)
        val calificacion = new Calificacion(usuario, evaluado, 0, LocalDate.now, "Prueba")
        this.guardar(calificacion)
        calificacion
    }


}