 # Changelog

 ## Done

 - Se unificaron los buscadores por nombre en `BuscarPorNombre`
 - Se agregaron tests de `BuscarPorNombre`
 - Movido `blanquearClave()` a AdmUsuarioAppModel porque no le corresponde estar en el repository

 ## To Do

 - Los repositories tienen lógica repetida. Extraer a un repository genérico usando Generics
