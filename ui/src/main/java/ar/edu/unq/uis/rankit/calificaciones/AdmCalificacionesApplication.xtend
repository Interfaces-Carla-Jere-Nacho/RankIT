package ar.edu.unq.uis.rankit.calificaciones

import org.uqbar.arena.Application
import org.uqbar.arena.windows.Window
import java.util.ArrayList

class AdmCalificacionesApplication extends Application {

    static def void main(String[] args) {
        new AdmCalificacionesApplication().start()
    }

    override protected Window<?> createMainWindow() {

        val calificaciones = new ArrayList<Calificacion>
        val RepoCalificaciones repo = new RepoCalificaciones(calificaciones)


        return new AdmCalificacionesWindow(this, new AdmCalificacionAppModel(repo))
    }
}