package ar.edu.unq.uis.rankit.evaluables


import org.uqbar.arena.windows.SimpleWindow
import org.uqbar.arena.windows.WindowOwner
import org.uqbar.arena.widgets.Panel
import org.uqbar.arena.widgets.Label
import org.uqbar.arena.layout.HorizontalLayout
import org.uqbar.arena.layout.ColumnLayout
import org.uqbar.arena.widgets.TextBox
import org.uqbar.arena.widgets.Button
import static extension org.uqbar.arena.xtend.ArenaXtendExtensions.*
import org.uqbar.arena.widgets.tables.Table
import org.uqbar.arena.widgets.tables.Column
import org.uqbar.arena.widgets.CheckBox
import java.awt.Color
import ar.edu.unq.uis.rankit.evaluables.AdmEvaluableAppModel
import ar.edu.unq.uis.rankit.evaluables.Evaluable
import ar.edu.unq.uis.rankit.calificaciones.AdmCalificacionesWindow
import org.uqbar.arena.bindings.NotNullObservable
import ar.edu.unq.uis.rankit.window.BaseLayout

class AdmEvaluablesWindow extends BaseLayout<Evaluable, AdmEvaluableAppModel>{

    private String tipoEvaluable

    new(WindowOwner parent, AdmEvaluableAppModel model, String tipoEvaluable) {
        super(parent, model)
        this.tipoEvaluable = tipoEvaluable
        title = "Rank-It --> Adm "+tipoEvaluable
        taskDescription = "Resumen de situacion: "
        modelObject.buscar

    }


    override crearPanelResumen(Panel mainPanel) {
        var panelResumen = new Panel(mainPanel)

        panelResumen.setLayout(new HorizontalLayout)
        crearLabelWithColor(panelResumen, tipoEvaluable+" inscriptos: ", "inscriptos", Color.BLUE)
        crearLabelWithColor(panelResumen, "    Habilitados: ", "habilitados", Color.BLUE)
        crearLabelWithColor(panelResumen, "    Deshabilitados: ", "deshabilitados", Color.RED)


        new Label(mainPanel) =>[
            text = tipoEvaluable
        ]
    }

    override createSearchPanel(Panel mainPanel) {
        var panelDeBusqueda = new Panel(mainPanel)

        panelDeBusqueda.setLayout(new HorizontalLayout)
        new Label(panelDeBusqueda) =>[
            text = "Buscar por nombre:"
        ]

        new TextBox(panelDeBusqueda) => [
            value <=> "buscadorEvaluable.nombre"
        ]

        new Button(panelDeBusqueda)
        .setCaption("Buscar")
        .onClick [ | modelObject.buscar ]
        .setAsDefault    }

    override createResultsGrid(Panel panelTabla) {
        new Table<Evaluable>(panelTabla, typeof(Evaluable)) => [
            numberVisibleRows = 7
            items <=> "evaluables"
            value <=> "evaluableSeleccionado"
            new Column<Evaluable>(it) => [
                title = "Fecha de registro"
                fixedSize = 150
                bindContentsToProperty("fechaRegistro")
            ]

            new Column<Evaluable>(it) => [
                title = "Nombre"
                fixedSize = 120
                bindContentsToProperty("nombre")
            ]
            new Column<Evaluable>(it) => [
                title = "Habilitado"
                fixedSize = 120
                bindContentsToProperty("habilitado").transformer =
                        [ Boolean recibe | if(recibe) "SI" else "NO" ]
            ]
        ]
    }

    override crearPanelEdicion(Panel panelEdicion) {
        var panelNombre = new Panel(panelEdicion)
        panelNombre.setLayout(new HorizontalLayout)
        new Label(panelNombre).text = "Nombre:"
        new Label(panelNombre) => [    value <=> "evaluableSeleccionado.nombre"]

        new Label(panelEdicion).text = "Edita la informacion" // Feedback

        new Label(panelEdicion).text = "Nombre:"
        var nombre = new TextBox(panelEdicion) => [
            value <=> "evaluableSeleccionado.nombre"
            width = 150]

        var panelHabilitado = new Panel(panelEdicion)
        panelHabilitado.setLayout(new HorizontalLayout)
        var habilitado = new CheckBox(panelHabilitado) => [
            value <=> "evaluableSeleccionadoHabilitado"]
        new Label(panelHabilitado).text = "Habilitado"

        var evaluableSeleccionado = new NotNullObservable("evaluableSeleccionado")


        var calificaciones = new Button(panelEdicion)
        .setCaption("Revisar calificaciones")
        .onClick [ | this.modelObject.filtrarCalificaciones
            new AdmCalificacionesWindow(this, this.modelObject.calificaciones).open
        ]
        var eliminar =new Button(panelEdicion)
        .setCaption("Eliminar")
        .onClick [ | modelObject.eliminarEvaluable]
        .bindEnabled(evaluableSeleccionado)


    }

}