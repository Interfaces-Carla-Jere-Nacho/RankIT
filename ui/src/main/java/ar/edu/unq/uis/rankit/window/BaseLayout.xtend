package ar.edu.unq.uis.rankit.window

import org.uqbar.arena.windows.SimpleWindow
import org.uqbar.arena.widgets.Panel
import org.uqbar.arena.windows.WindowOwner
import ar.edu.unq.uis.rankit.appModels.BaseAppModel
import static extension org.uqbar.arena.xtend.ArenaXtendExtensions.*
import org.uqbar.arena.windows.WindowOwner
import org.uqbar.arena.widgets.Panel
import org.uqbar.arena.widgets.Label
import org.uqbar.arena.widgets.TextBox
import org.uqbar.arena.widgets.Button
import org.uqbar.arena.widgets.tables.Table
import ar.edu.unq.uis.rankit.usuarios.Usuario
import org.uqbar.arena.widgets.tables.Column
import org.uqbar.arena.layout.HorizontalLayout
import org.uqbar.arena.widgets.CheckBox
import ar.edu.unq.uis.rankit.usuarios.AdmUsuarioAppModel
import java.awt.Color
import ar.edu.unq.uis.rankit.calificaciones.AdmCalificacionesWindow
import org.uqbar.arena.windows.ErrorsPanel
import org.uqbar.arena.layout.VerticalLayout
import org.uqbar.arena.bindings.NotNullObservable
import ar.edu.unq.uis.rankit.window.BaseLayout

/**
 * Created by Sarappa Carla on 22/09/16 10:13.
 */
abstract class BaseLayout<M, T extends BaseAppModel<M>> extends SimpleWindow<T>{

    new (WindowOwner parent, T model) {
        super(parent, model)
    }

    override createFormPanel(Panel mainPanel) {
        crearPanelResumen(mainPanel)
        createSearchPanel(mainPanel)
        createBody(mainPanel)
    }

    abstract def void crearPanelResumen(Panel mainPanel)
    abstract def void createSearchPanel(Panel mainPanel)


    def createBody(Panel panel){
        val body = new Panel(panel) => [
            layout = new HorizontalLayout
        ]
        var panelTabla = new Panel(body)
        panelTabla.layout = new VerticalLayout
        createResultsGrid(panelTabla)
        nuevaEntry(panelTabla)

        crearPanelEdicion(new Panel(body) => [
            layout = new VerticalLayout
        ]
        )


    }

    abstract def void createResultsGrid(Panel panelTabla)
    abstract def void crearPanelEdicion(Panel body)

    override protected addActions(Panel mainPanel) {
    }



    def nuevaEntry(Panel actionPanel){
        new Button(actionPanel)
        .setCaption("Nuevo")
        .onClick [ | modelObject.nuevaEntidad]

        new Label(actionPanel) =>[
            width = 468 ]
    }

    def crearLabelWithColor(Panel searchPanel, String label, String binding, Color color) {
        new Label(searchPanel).text = label
        new Label(searchPanel) => [
            foreground = color
            value <=> binding
        ]
    }

}