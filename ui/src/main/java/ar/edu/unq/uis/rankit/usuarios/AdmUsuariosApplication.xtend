package ar.edu.unq.uis.rankit.usuarios

import org.uqbar.arena.Application
import org.uqbar.arena.windows.Window
import java.util.ArrayList
import java.util.List
import ar.edu.unq.uis.rankit.calificaciones.Calificacion
import ar.edu.unq.uis.rankit.calificaciones.RepoCalificaciones
import ar.edu.unq.uis.rankit.calificaciones.AdmCalificacionAppModel

class AdmUsuariosApplication extends Application {

    static def void main(String[] args) {
        new AdmUsuariosApplication().start()
    }

    override protected Window<?> createMainWindow() {
        val List<Usuario> usuarios = new ArrayList
        val RepoUsuarios repo = new RepoUsuarios(usuarios)

        var calificacion = new ArrayList<Calificacion>
        var repoC = new RepoCalificaciones(calificacion)
        var calificaciones = new AdmCalificacionAppModel(repoC)

        return new AdmUsuariosWindow(this, new AdmUsuarioAppModel(repo, calificaciones))
    }
}