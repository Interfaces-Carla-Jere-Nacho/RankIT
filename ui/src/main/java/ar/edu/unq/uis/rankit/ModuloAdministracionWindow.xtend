package ar.edu.unq.uis.rankit

import org.uqbar.arena.widgets.Panel;
import org.uqbar.arena.windows.SimpleWindow;
import org.uqbar.arena.windows.WindowOwner;
import org.uqbar.arena.widgets.Label
import org.uqbar.arena.widgets.Button
import ar.edu.unq.uis.rankit.usuarios.AdmUsuariosWindow
import org.uqbar.arena.layout.ColumnLayout
import ar.edu.unq.uis.rankit.calificaciones.AdmCalificacionesWindow
import static extension org.uqbar.arena.xtend.ArenaXtendExtensions.*
import org.uqbar.arena.layout.HorizontalLayout
import ar.edu.unq.uis.rankit.evaluables.AdmEvaluablesWindow

public class ModuloAdministracionWindow extends SimpleWindow<RankItAppModel> {

    new (WindowOwner parent, RankItAppModel model)
    {
        super(parent, model);
        title = "Rank-It"
    }

    override def createMainTemplate(Panel mainPanel) {
        mainPanel.layout = new ColumnLayout(1)
        new Label(mainPanel).text ="\nRank-It! Modulo de administracion"
        new Label(mainPanel).text = "\nDesde este modulo vas a poder gestionar los datos y opciones de la aplicacion,"
        new Label(mainPanel).text = "\ncomo sos una persona de confianza vas a tener acceso a todo."
        new Label(mainPanel).text = "\nSiempre acordate: 'Con un gran poder viene una gran responsabilidad'\n"

        val botones = new Panel(mainPanel)
        botones.setLayout(new HorizontalLayout)

        val AdmUsuariosWindow usuariosWindow = new AdmUsuariosWindow(this, this.modelObject.usuarios)
        val AdmCalificacionesWindow calificacionesWindow = new AdmCalificacionesWindow(this, this.modelObject.calificaciones)
        val AdmEvaluablesWindow serviciosWindow = new AdmEvaluablesWindow(this, this.modelObject.servicios, "Servicios")
        val AdmEvaluablesWindow lugaresWindow = new AdmEvaluablesWindow(this, this.modelObject.lugares, "Lugares")

        new Button(botones)
        .setCaption("Adm. Usuarios")
        .onClick [ | new AdmUsuariosWindow(this, this.modelObject.usuarios).open ]

        new Button(botones)
        .setCaption("Adm. Calificaciones")
        .onClick [ | new AdmCalificacionesWindow(this, this.modelObject.calificaciones).open ]

        new Button(botones)
        .setCaption("Adm. Servicios")
        .onClick [ | new AdmEvaluablesWindow(this, this.modelObject.servicios, "Servicios").open ]

        new Button(botones)
        .setCaption("Adm. Lugares")
        .onClick [ | new AdmEvaluablesWindow(this, this.modelObject.lugares, "Lugares").open ]


        val resumen = new Panel(mainPanel)
        resumen.setLayout(new HorizontalLayout)
        new Label(resumen) =>[width = 40]
        crearLabel(resumen, "usuarios.activos", " / ", "usuarios.registrados")
        new Label(resumen) =>[width = 100]
        crearLabel(resumen, "calificaciones.inofensivas", " / ", "calificaciones.registradas")
        new Label(resumen) =>[width = 110]
        crearLabel(resumen, "servicios.habilitados", " / ", "servicios.inscriptos")
        new Label(resumen) =>[width = 90]
        crearLabel(resumen, "lugares.habilitados", " / ", "lugares.inscriptos")
    }

    def crearLabel(Panel panel, String binding1, String label, String binding2) {
        new Label(panel) => [
            value <=> binding1
        ]

        new Label(panel).text = label

        new Label(panel) => [
            value <=> binding2
        ]
    }


    override addActions(Panel actionsPanel) {
    }

    override createFormPanel(Panel mainPanel) {
    }

}