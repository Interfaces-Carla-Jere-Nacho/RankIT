package ar.edu.unq.uis.rankit.calificaciones


import static extension org.uqbar.arena.xtend.ArenaXtendExtensions.*
import org.uqbar.arena.windows.SimpleWindow
import org.uqbar.arena.windows.WindowOwner
import org.uqbar.arena.widgets.Panel
import org.uqbar.arena.widgets.Label
import org.uqbar.arena.layout.ColumnLayout
import org.uqbar.arena.widgets.TextBox
import org.uqbar.arena.widgets.Button
import org.uqbar.arena.widgets.tables.Table
import org.uqbar.arena.widgets.tables.Column
import org.uqbar.arena.layout.HorizontalLayout
import org.uqbar.arena.widgets.CheckBox
import java.awt.Color
import org.uqbar.arena.bindings.NotNullObservable

class AdmCalificacionesWindow extends SimpleWindow<AdmCalificacionAppModel>{

    new(WindowOwner parent, AdmCalificacionAppModel model) {
        super(parent, model)
        title = "Rank-It --> Adm Calificaciones"
        modelObject.buscar
    }

    override def createMainTemplate(Panel mainPanel) {
        mainPanel.layout = new ColumnLayout(1)
        new Label(mainPanel).text = "Resumen de situacion: "

        //Panel donde se muestra la cantidad de calificaciones y cuantas son ofensivas

        var panelResumen = new Panel(mainPanel)

        panelResumen.setLayout(new HorizontalLayout)
        crearLabelWithColor(panelResumen, "Calificaciones Registradas: ", "registradas", Color.BLUE)
        crearLabelWithColor(panelResumen, "    Ofensivas: ", "ofensivas", Color.RED)

        new Label(mainPanel) =>[
            text = "Calificaciones"
        ]



        var panelDeBusqueda = new Panel(mainPanel)

        new Label(panelDeBusqueda) =>[
            text = "Usuario"
        ]

        panelDeBusqueda.setLayout(new HorizontalLayout)
        new TextBox(panelDeBusqueda) => [
            value <=> "buscadorCalificacion.user"
        ]

        new Label(panelDeBusqueda) =>[
            text = "Evaluado"
        ]

        new TextBox(panelDeBusqueda) => [
            value <=> "buscadorCalificacion.evaluado"
        ]

        new Button(panelDeBusqueda)
        .setCaption("Buscar")
        .onClick [ | modelObject.buscar ]
        .setAsDefault


        var panel = new Panel(mainPanel)
        panel.setLayout(new HorizontalLayout)
        //Panel que contiene el panel para la tabla y el panel de edicion

        var panelTabla = new Panel(panel)
        //Panel para la tabla
        new Table<Calificacion>(panelTabla, typeof(Calificacion)) => [
            numberVisibleRows = 12
            items <=> "calificaciones"
            value <=> "calificacionSeleccionada"

            //Definimos las columnas
            new Column<Calificacion>(it) => [
                title = "Evaluado"
                fixedSize = 150
                bindContentsToProperty("evaluado.nombre")
            ]

            new Column<Calificacion>(it) => [
                title = "Ptos"
                fixedSize = 75
                bindContentsToProperty("puntos")
            ]

            new Column<Calificacion>(it) => [
                title = "Fecha de registro"
                fixedSize = 150
                bindContentsToProperty("fechaRegistro")
            ]

            new Column<Calificacion>(it) => [
                title = "User"
                fixedSize = 150
                bindContentsToProperty("user.nombre")
            ]

            new Column<Calificacion>(it) => [
                title = "Es Ofensiva"
                fixedSize = 75
                bindContentsToProperty("esOfensiva").transformer =
                        [ Boolean recibe | if (recibe) "SI" else "NO" ]
            ]
        ]

        var panelEdicion = new Panel(panel)
        panelEdicion.layout = new ColumnLayout(1)
        new Label(panelEdicion).text = "Edita la informacion" // Feedback
        new Label(panelEdicion).text = "Evaluado:"
        new Label(panelEdicion) => [	value <=> "calificacionSeleccionada.evaluado.nombre"]


        var panelFecha = new Panel(panelEdicion)
        panelFecha.setLayout(new HorizontalLayout)
        new Label(panelFecha).text = "Fecha: "
        new Label(panelFecha) => [	value <=> "calificacionSeleccionada.fechaRegistro"]

        var panelUsuario = new Panel(panelEdicion)
        panelUsuario.setLayout(new HorizontalLayout)
        new Label(panelUsuario).text = "Usuario: "
        new Label(panelUsuario) => [	value <=> "calificacionSeleccionada.user.nombre"]

        new Label(panelEdicion).text = "Puntaje:"
        var puntaje = new TextBox(panelEdicion) => [
            value <=> "calificacionSeleccionada.puntos"
            width = 200]

        new Label(panelEdicion).text = "Detalle:"
        var detalle = new TextBox(panelEdicion) => [
            value <=> "calificacionSeleccionada.detalle"
            width = 200]

        var panelOfensivo = new Panel(panelEdicion)
        panelOfensivo.setLayout(new HorizontalLayout)
        var ofensivo = new CheckBox(panelOfensivo) => [
            value <=> "calificacionSeleccionada.esOfensiva"]
        new Label(panelOfensivo).text = "Contenido Ofensivo"


        var eliminar = new Button(panelEdicion)
        .setCaption("Eliminar")
        .onClick [ | modelObject.eliminarCalificacion]

        // Deshabilitar los botones si no hay ningún elemento seleccionado en la grilla.
        var calificacionSeleccionada = new NotNullObservable("calificacionSeleccionada")
        eliminar.bindEnabled(calificacionSeleccionada)
        ofensivo.bindEnabled(calificacionSeleccionada)
        detalle.bindEnabled(calificacionSeleccionada)
        puntaje.bindEnabled(calificacionSeleccionada)

        var actionPanel = new Panel(mainPanel)
        actionPanel.setLayout(new HorizontalLayout)

        new Button(actionPanel)
        .setCaption("Nuevo")
        .onClick [ | modelObject.nuevaCalificacion]
    }




    override protected createFormPanel(Panel mainPanel) {
    }

    def crearLabel(Panel searchPanel, String label, String binding) {
        new Label(searchPanel).text = label
        new Label(searchPanel) => [
            value <=> binding
        ]
    }

    def crearLabelWithColor(Panel searchPanel, String label, String binding, Color color) {
        new Label(searchPanel).text = label
        new Label(searchPanel) => [
            foreground = color
            value <=> binding
        ]
    }

    override protected addActions(Panel mainPanel) {
    }
}