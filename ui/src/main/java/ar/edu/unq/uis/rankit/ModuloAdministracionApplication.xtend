package ar.edu.unq.uis.rankit

import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.arena.Application
import org.uqbar.arena.windows.Window
import ar.edu.unq.uis.rankit.usuarios.RepoUsuarios
import ar.edu.unq.uis.rankit.calificaciones.RepoCalificaciones
import java.time.LocalDate
import java.util.List
import ar.edu.unq.uis.rankit.usuarios.Usuario
import java.util.ArrayList
import java.util.Arrays
import ar.edu.unq.uis.rankit.calificaciones.Calificacion
import ar.edu.unq.uis.rankit.evaluables.Evaluable
import ar.edu.unq.uis.rankit.evaluables.RepoEvaluable

@Accessors
class ModuloAdministracionApplication extends Application {

    static def void main(String[] args) {
        new ModuloAdministracionApplication().start()
    }

    override protected Window<?> createMainWindow() {

        val fecha = LocalDate.now
        //Inicializamos un repo de usuarios con una lista de usuarios
        val List<Usuario> usuarios = new ArrayList
        val denna = new Usuario("Denna", fecha)
        val tincho = new Usuario("Tincho", fecha)
        val gise = new Usuario("Gise", fecha)
        val nacho = new Usuario("Nacho", fecha)
        val juancho = new Usuario("Juancho", fecha)
        val tefi = new Usuario("Tefi", fecha)
        val joaco = new Usuario("Joaco", fecha)
        val roco = new Usuario("Roco", fecha)
        val luz = new Usuario("Luz", fecha)
        val andy = new Usuario("Andy", fecha)
        usuarios.addAll(Arrays.asList(denna, tincho, gise, nacho, juancho, tefi, joaco,
        roco, luz, andy))
        val RepoUsuarios repoU = new RepoUsuarios(usuarios)

        //Inicializamos repoServicios con lista de servicios
        val servicios  =new ArrayList<Evaluable>
        val speedy = new Evaluable("Speedy", fecha)
        val plomerojuan = new Evaluable("Plomero Juan", fecha)
        val elecPepe = new Evaluable("Electricidad Pepe", fecha)
        val cerrajeriaJose = new Evaluable("Cerrajeria Jose", fecha)
        val aysa = new Evaluable("Aysa", fecha)
        servicios.addAll(Arrays.asList(speedy, plomerojuan, elecPepe, cerrajeriaJose, aysa))
        val repoL = new RepoEvaluable(servicios)

        //Inicializamos un repo de lugares con una lista de lugares
        var lugares = new ArrayList<Evaluable>

        val jobs = new Evaluable("Jobs", fecha)
        val sushi = new Evaluable("Sushi Pop", fecha)
        val okinawa = new Evaluable("Okinawa", fecha)
        val constitucion = new Evaluable("Constitucion", fecha)
        val freddo = new Evaluable("Freddo", fecha)
        val puertoMadero = new Evaluable("Puerto Madero", fecha)
        val hoyts = new Evaluable("Hoyts Abasto", fecha)
        val reserva = new Evaluable("Reserva Ecologica", fecha)
        lugares.addAll(Arrays.asList(jobs, sushi, okinawa, constitucion, freddo,
        puertoMadero, hoyts, reserva))
        var repoS = new RepoEvaluable(lugares)


        //Inicializamos un repo de calificaciones con una lista de calificaciones.
        val List<Calificacion> calificaciones = new ArrayList
        calificaciones.add(new Calificacion(denna, plomerojuan, 8, fecha, "Trabaja bien"))
        calificaciones.add(new Calificacion(gise, sushi, 4, fecha, "Aceptable"))
        calificaciones.add(new Calificacion(nacho, sushi, 2, fecha, "Horrible"))
        calificaciones.add(new Calificacion(juancho, sushi, 5, fecha, "Excelente"))
        calificaciones.add(new Calificacion(tefi, plomerojuan, 0, fecha, "Impuntual"))
        calificaciones.add(new Calificacion(joaco, jobs, 2, fecha, "Es carisimo"))
        calificaciones.add(new Calificacion(roco, elecPepe, 1, fecha, "No saben nada"))
        calificaciones.add(new Calificacion(luz, speedy, 0, fecha, "Filtran el trafico"))
        calificaciones.add(new Calificacion(andy, speedy, 2, fecha, "Se corta"))
        val RepoCalificaciones repoC = new RepoCalificaciones(calificaciones)

        val RankItAppModel modulo = new RankItAppModel(repoU, repoC, repoL, repoS)


        return new ModuloAdministracionWindow(this, modulo)
    }
}