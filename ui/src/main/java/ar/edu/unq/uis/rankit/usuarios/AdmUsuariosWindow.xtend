package ar.edu.unq.uis.rankit.usuarios

import static extension org.uqbar.arena.xtend.ArenaXtendExtensions.*
import org.uqbar.arena.windows.WindowOwner
import org.uqbar.arena.widgets.Panel
import org.uqbar.arena.widgets.Label
import org.uqbar.arena.widgets.TextBox
import org.uqbar.arena.widgets.Button
import org.uqbar.arena.widgets.tables.Table
import ar.edu.unq.uis.rankit.usuarios.Usuario
import org.uqbar.arena.widgets.tables.Column
import org.uqbar.arena.layout.HorizontalLayout
import org.uqbar.arena.widgets.CheckBox
import ar.edu.unq.uis.rankit.usuarios.AdmUsuarioAppModel
import java.awt.Color
import ar.edu.unq.uis.rankit.calificaciones.AdmCalificacionesWindow
import org.uqbar.arena.windows.ErrorsPanel
import org.uqbar.arena.layout.VerticalLayout
import org.uqbar.arena.bindings.NotNullObservable
import ar.edu.unq.uis.rankit.window.BaseLayout

class AdmUsuariosWindow extends BaseLayout<Usuario, AdmUsuarioAppModel>{

    new(WindowOwner parent, AdmUsuarioAppModel model) {
        super(parent, model)
        modelObject.buscar

    }

    override def createMainTemplate(Panel mainPanel) {
        title = "Rank-It --> Adm Usuarios"
        taskDescription = "Resumen de situacion: "
        super.createMainTemplate(mainPanel)

    }

    //Panel para ver las estadisticas de los usuarios registrados
    override def crearPanelResumen(Panel mainPanel){
        var panelResumen = new Panel(mainPanel)
        panelResumen.setLayout(new HorizontalLayout)



        crearLabelWithColor(panelResumen, "Usuarios registrados: ", "registrados",Color.BLUE)
        crearLabelWithColor(panelResumen, "    Activos: ", "activos", Color.BLUE)
        crearLabelWithColor(panelResumen, "    Inactivos: ", "inactivos", Color.RED)
        crearLabelWithColor(panelResumen, "    Baneados: ", "baneados", Color.RED)
        new Label(mainPanel) =>[
            text = "Usuarios"
        ]
    }

    override def createSearchPanel(Panel mainPanel){

        mainPanel.layout = new VerticalLayout


        val panelDeBusqueda = new Panel(mainPanel) => [
            layout = new HorizontalLayout
        ]

        new Label(panelDeBusqueda) =>[
            text = "Buscar por nombre de usuario:"
        ]

        new TextBox(panelDeBusqueda) => [
            value <=> "buscaUsuario.nombre"
        ]

        new Button(panelDeBusqueda) => [
            caption = "Buscar"
            onClick([| modelObject.buscar])
            setAsDefault
            disableOnError
        ]
    }


    def acciones (Panel actionsPanel) {
        val usuarioSeleccionado = new NotNullObservable("usuarioSeleccionado")
//        fecha.bindEnabled(usuarioSeleccionado)
//        activo.bindEnabled(usuarioSeleccionado)
//        baneado.bindEnabled(usuarioSeleccionado)
//        blanquear.bindEnabled(usuarioSeleccionado)
//        calificaciones.bindEnabled(usuarioSeleccionado)

        new Button(actionsPanel) => [
            caption = "Revisar calificaciones"
            onClick([ | this.modelObject.filtrarCalificaciones
                new AdmCalificacionesWindow(this, this.modelObject.calificaciones).open])
        ]

        new Button(actionsPanel) => [
            caption = "Blanquear clave"
            onClick([|modelObject.blanquearClave])
        ]

        new Button(actionsPanel) => [
            caption = "Eliminar"
            onClick([|modelObject.eliminar])
            bindEnabled(usuarioSeleccionado)
        ]

    }

    override def crearPanelEdicion(Panel panelEdicion){


        var panelNombre = new Panel(panelEdicion)
        panelNombre.setLayout(new HorizontalLayout)
        new Label(panelNombre).text = "Nombre:"
        new Label(panelNombre) => [    value <=> "usuarioSeleccionado.nombre"]

        val feedbackPanel = new Panel(panelEdicion)
        new ErrorsPanel(feedbackPanel, "Edita la informacion")// Feedback


        new Label(panelEdicion).text = "Fecha de registro:"
        new TextBox(panelEdicion) => [
            value <=> "usuarioSeleccionado.fechaRegistro"
            width = 150]

        var panelActivo = new Panel(panelEdicion)
        panelActivo.setLayout(new HorizontalLayout)
        new CheckBox(panelActivo) => [
            value <=> "usuarioSeleccionado.activo"
            bindEnabledToProperty("usuarioSeleccionado.noBaneado")]
        new Label(panelActivo).text = "Activo"

        var panelBaneado = new Panel(panelEdicion)
        panelBaneado.setLayout(new HorizontalLayout)
        new CheckBox(panelBaneado) => [
            value <=> "usuarioSeleccionado.baneado"]
        new Label(panelBaneado).text = "Baneado"

        new Label(panelEdicion).text = "Ultima publicacion:"
        new Label(panelEdicion) => [    value <=> "usuarioSeleccionado.ultimaFechaDePublicacion"]

        acciones(panelEdicion)
    }


    override def createResultsGrid(Panel panelTabla){

        //Creamos la tabla para mostrar los datos de los usuarios
        new Table<Usuario>(panelTabla, typeof(Usuario)) => [
            numberVisibleRows = 9
            //bindeamos los datos
            items <=> "usuarios"
            value <=> "usuarioSeleccionado"

            //Definimos las columnas
            new Column<Usuario>(it) => [
                title = "Fecha de registro"
                fixedSize = 200
                bindContentsToProperty("fechaRegistro")
            ]

            new Column<Usuario>(it) => [
                title = "Nombre"
                fixedSize = 100
                bindContentsToProperty("nombre")
            ]
            new Column<Usuario>(it) => [
                title = "Activo"
                fixedSize = 100
                bindContentsToProperty("activo").transformer =
                        [ Boolean recibe | if(recibe) "SI" else "NO" ]
            ]

            new Column<Usuario>(it) => [
                title = "Baneado"
                fixedSize = 100
                bindContentsToProperty("baneado").transformer =
                        [ Boolean recibe | if(recibe) "SI" else "--" ]
            ]

        ]

    }



    //
    ////        def openDialog(Dialog<?> dialog) {
    ////    dialog.onAccept[ | modelObject.search ]
    ////    dialog.open
    ////}
    //
    //


    override addActions(Panel panel) {
    }






}