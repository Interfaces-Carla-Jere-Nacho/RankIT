package ar.edu.unq.uis.rankit.evaluables

import org.uqbar.arena.Application
import org.uqbar.arena.windows.Window
import java.util.ArrayList
import ar.edu.unq.uis.rankit.evaluables.RepoEvaluable
import ar.edu.unq.uis.rankit.evaluables.Evaluable
import ar.edu.unq.uis.rankit.evaluables.AdmEvaluableAppModel
import ar.edu.unq.uis.rankit.calificaciones.RepoCalificaciones
import ar.edu.unq.uis.rankit.calificaciones.Calificacion
import ar.edu.unq.uis.rankit.calificaciones.AdmCalificacionAppModel

class AdmEvaluablesApplication extends Application {

    static def void main(String[] args) {
        new AdmEvaluablesApplication().start()
    }

    override protected Window<?> createMainWindow() {
        var lugares = new ArrayList<Evaluable>
        var repo = new RepoEvaluable(lugares)

        var calificacion = new ArrayList<Calificacion>
        var repoC = new RepoCalificaciones(calificacion)
        var calificaciones = new AdmCalificacionAppModel(repoC)

        return new AdmEvaluablesWindow(this, new AdmEvaluableAppModel(repo, calificaciones), "Servicios")
    }
}